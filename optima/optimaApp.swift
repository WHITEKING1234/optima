//
//  optimaApp.swift
//  optima
//
//  Created by Mac on 27/5/23.
//

import SwiftUI

@main
struct optimaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
